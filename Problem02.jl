### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 2607282a-a55e-47e2-a42e-c7a06d20aa6b
using PlutoUI

# ╔═╡ 7d7f1ce0-aedd-11eb-3163-9148c2327fd0
struct TerminalNode
utility::Dict
end

# ╔═╡ 0c89bdb7-751d-4336-ac1f-3d952a059542
struct playerAction
	move::String
end

# ╔═╡ a60b8ba9-cdc3-40aa-93b2-755ac2c59298
struct playerNode
action::playerAction
nodes::Vector{Union{TerminalNode,playerNode}}
end

# ╔═╡ 81dc8bd6-66ec-4652-9ab6-4676aae2454c
struct Game
initial::playerNode
	function Game(initialState::playerNode)
		return new("A", Dict([
                            Pair("A", Dict("A1"=>"B",  "A2"=>"C",  "A3"=>"D")),
                            Pair("B", Dict("B1"=>"B1", "B2"=>"B2", "B3"=>"B3")),]),
                            
                        Dict([
                            Pair("B1", 2),
                            Pair("B2", 14),
                            Pair("B3", 8),
                            ]));
	end
end

# ╔═╡ 63745828-ba30-4c25-a8ba-22ee11605f6b
begin
	print("Enter total number of terminal node\n")
	n=readline()
	tN=parse(Int64, n)
	
	terminal_node=[]
	player_node=[]
	
	for i=1:tN
		print("Enter the value of your terminal node ",i,"?\n")
		nV=readline()
		push!(terminal_node,nV)
	end
	
	print("Enter total number of player node\n")
	p=readline()
	pN=parse(Int64,p)
	
	
	for i=1:pN
		print("Enter the value of your player node ",i,"?\n")
		pV=readline()
		push!(player_node,pV)
	end
end

# ╔═╡ e3477d4d-fed0-4563-9e96-5b4d8d46aa6d
function actions(game::Game, state::playerNode)
     return collect(keys(get(game.nodes, state, Dict())));
end

# ╔═╡ f8b587e1-caeb-4d3a-8cb3-c8e1cd0afb3a
function result(game::Game, state::playerNode, move::String)
    return game.nodes[state][move];
end

# ╔═╡ 095b3a02-b20c-4dea-894d-fcf6d898c082
function utility(game::Game, state::playerNode, player::String)
    if (player == "MAX")
        return game.utility[state];
    else
        return -game.utility[state];
	end
end

# ╔═╡ 82eab368-304f-409f-808f-055a168f7536
function terminal_test(game::Game, state::playerNode)
    return !(state in ["A", "B"]);
end

# ╔═╡ d8045283-8ed1-4a29-b364-a9002d0a3d0c
function to_move(game::Game, state::playerNode)
    return if_((state in ["B"]), "MIN", "MAX");
end

# ╔═╡ c61765d4-995e-4274-840c-ede0e6ce4613
function display(game::TerminalNode, state::playerNode)
    println(state);
end

# ╔═╡ 2c76428d-2786-4b82-9b0f-2ea62c7587e0
#= function alphaBeta(game::Game, node::TerminalNode, state::playerNode)
	if playerNode <= 0
		game.action(state)
		isLeaf, move <=maxValue(game,state, -inifinity,+inifinity)
		return move
		alphaBeta(0, 0, true, -inifinity, +inifinity)
	end
end
=#

# ╔═╡ f5355017-4afc-4f38-88d8-980ccdd613cd
begin
	function alphabeta_max_value(game::Game, player::String, state::playerNode,           alpha::Number, beta::Number)
	if (terminal_test(game, state))
		return utility(game, state, player)
	end
	local v::Float64 = -Int64;
	for action in actions(game, state)
		v = max(v, alphabeta_min_value(game, player, result(game, state, action),         alpha, beta));
        if (v >= beta)
            return v;
        end
        alpha = max(alpha, v);
	end
	return v;
   end
	function alphabeta_min_value(game::Game, player::String, state::playerNode,           alpha::Number, beta::Number)
    if (terminal_test(game, state))
        return utility(game, state, player);
    end
    local v::Float64 = Int64;
    for action in actions(game, state)
        v = min(v, alphabeta_max_value(game, player, result(game, state, action),         alpha, beta));
        if (v <= alpha)
            return v;
        end
        beta = min(beta, v);
    end
    return v;
	end
end

# ╔═╡ a81a71d0-d193-49c7-8144-76576ca3c34f
#= function maxValue(game::Game,node::TerminalNode, alpha::Int, beta::Int, player::playerNode, depth::Int, side::Int)
	println("visit:",node,",alpha=$alpha")
    return A(utility,move)pair
	if game.TerminalNode(state)
		return game.Utility(state,playerNode),null
		vector <= -inifinity
		for i in game.Actions(state)
			
		vector2,i2<=minValue(game,game.Result(state,i),alpha,bata)
				if vector2 > vector
					vector,move<=vector, i
					alpha<=MAX(alpha, vector)
					if vector >= beta
						return vector, move
					end
						return vector,move
				end
		end
	end
end
=#
				

# ╔═╡ 4b279be9-b8da-485e-a2be-8e8afed7d6e5
#=function minValue(game::Game,node::TerminalNode, alpha::Int, beta::Int, player::playerNode, depth::Int, side::Int)
	println("visit:",node,",beta=$beta")
    return A(utility,move)pair
	if game.TerminalNode(state)
		return game.Utility(state,playerNode),null
		vector <= +inifinity
		for i in game.Actions(state)
			
	    vector2,i2<=minValue(game,game.Result(state,i),alpha,bata)
				if vector2 > vector
					vector,move<=vector, i
					beta<=MIN(beta, vector)
					if vector <= beta
						return vector, move
					end
						return vector,move
				end
		end
	end
end
=#
				

# ╔═╡ 2ce9991f-b4a8-40ed-bace-246301440a22
#= heuristicMinimax(s,d) = {Eval(s,MAX)
if IsCutOff(s,d)
	maxActions(s)heuristicMinimax(Result(s,A),d+1)
	if move(s) = MAX
		minActions(s)heuristicMinimax(s,A),d+1)
		if move(s) = MIN}
		end
	end
end
=#

# ╔═╡ 25d3cc76-cb82-4391-a7cb-6ee6aefa0198
#find the best action using alpha-beta pruning 
function alphabeta_search(state::playerNode, game::Game)
	local player::String = to_move(game, state);
    return argmax(actions(game, state), 
                    (function(action::String,; relevant_game::Game=game, relevant_state::String=state, relevant_player::String=player)
                        return alphabeta_min_value(relevant_game, relevant_player, result(relevant_game, relevant_state, action), -Int64, Int64);
                    end));
end

# ╔═╡ afa84015-d3ed-4003-a3f1-bf032a86b64a
function game_player(game::Game, state::String)
    return alphabeta_search(state, game);
end

# ╔═╡ 7bbf1ee8-7c80-4241-b08d-cad010c1cdb5
function play_game(game::Game, players::Vararg{Function})
    state = game.initial;
    while (true)
        for player in players
            move = player(game, state);
            state = result(game, state, move);
            if (terminal_test(game, state))
                return utility(game, state, to_move(game, game.initial));
            end
        end
    end
end

# ╔═╡ 346ed276-48d4-41f2-8fd7-a7918f029aa6
function generate(node::playerNode)
    children = get(tree,node,[])
    children
end


# ╔═╡ 58bd1c11-26e5-490f-9761-410646baeb38
function Eval(s,p)
	totalCount = 0
	totalCount += checkComplete(s,(:s1, p.config[s1]),
		(:s2, p.config[2]))
	return totalCount
end

# ╔═╡ 35fd36f4-4af2-4312-b2e0-706a8f17dec0
function solveGame(game::Game)
	Eval(game.initial)
end

# ╔═╡ Cell order:
# ╠═2607282a-a55e-47e2-a42e-c7a06d20aa6b
# ╠═7d7f1ce0-aedd-11eb-3163-9148c2327fd0
# ╠═0c89bdb7-751d-4336-ac1f-3d952a059542
# ╠═a60b8ba9-cdc3-40aa-93b2-755ac2c59298
# ╠═81dc8bd6-66ec-4652-9ab6-4676aae2454c
# ╠═63745828-ba30-4c25-a8ba-22ee11605f6b
# ╠═e3477d4d-fed0-4563-9e96-5b4d8d46aa6d
# ╠═f8b587e1-caeb-4d3a-8cb3-c8e1cd0afb3a
# ╠═095b3a02-b20c-4dea-894d-fcf6d898c082
# ╠═82eab368-304f-409f-808f-055a168f7536
# ╠═d8045283-8ed1-4a29-b364-a9002d0a3d0c
# ╠═c61765d4-995e-4274-840c-ede0e6ce4613
# ╠═2c76428d-2786-4b82-9b0f-2ea62c7587e0
# ╠═f5355017-4afc-4f38-88d8-980ccdd613cd
# ╠═a81a71d0-d193-49c7-8144-76576ca3c34f
# ╠═4b279be9-b8da-485e-a2be-8e8afed7d6e5
# ╠═2ce9991f-b4a8-40ed-bace-246301440a22
# ╠═25d3cc76-cb82-4391-a7cb-6ee6aefa0198
# ╠═afa84015-d3ed-4003-a3f1-bf032a86b64a
# ╠═7bbf1ee8-7c80-4241-b08d-cad010c1cdb5
# ╠═346ed276-48d4-41f2-8fd7-a7918f029aa6
# ╠═58bd1c11-26e5-490f-9761-410646baeb38
# ╠═35fd36f4-4af2-4312-b2e0-706a8f17dec0
