### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ d555e035-af6b-4eaa-ae4d-b3f97118101c
using PlutoUI

# ╔═╡ 63bafe4c-7fe4-45d0-84b8-9b0853c6aead
using DataStructures

# ╔═╡ 14e0e8ce-4ee4-44a9-9df1-33188fa99275
println("----------")

# ╔═╡ 88848721-d4b0-4419-80df-4d5a0acc1566
println("VERBAL")

# ╔═╡ 7661c0ba-2df8-4c03-9188-d7bf8ae2b8bb
println("----------")

# ╔═╡ eb07a06e-5c37-4bc1-94ea-6e3ee0bcb09e
# defining State
struct State
    name::String
    position::Int64
    item::Vector{Bool}
end

# ╔═╡ 34e42f53-4040-4a12-bf27-e008acddbbe5
#defining Action
struct Action
    name::String
    cost::Int64
end

# ╔═╡ 5bab5f4f-26ef-4c63-ba66-0d7be96decbe
# creating Actions
move_east = Action("me",3)

# ╔═╡ 288f6783-a1e8-40db-a05e-12b3ddc520b3
move_west = Action("mw",3)

# ╔═╡ 14a04b6d-95a9-443b-a99a-501e7424a079
remain = Action("re",1)

# ╔═╡ 133ae44b-9ce1-49e1-aea7-aed58556d880
collect = Action("co",5)

# ╔═╡ 6a6d5473-6d98-430b-ac92-cc80b19dbc95
# creating states

# ╔═╡ e35ce7fb-8f05-437b-9d96-fceec2b10eae
state1 = State("W",1,[true,false,false])

# ╔═╡ f8942517-95d8-4158-b23e-7e31235e5e8b
state2 = State("C1",2,[true,true,true])

# ╔═╡ 9c9389df-fe76-4a39-abe4-548847b42ea2
state3 = State("C2",3,[false,true,true])

# ╔═╡ 710531e8-12c0-40a4-aa7f-302184f7ce86
state4 = State("E",4,[false,true,false])

# ╔═╡ 9ecf52b7-c41c-4825-a1d9-50b54f61a9d8
# adding more states result

# ╔═╡ c52714ae-ebce-4917-9e94-6b5beff822c7
state5 = State("WDestination",1,[false,false,false])

# ╔═╡ b5a9c705-e564-4ade-ac50-b4c8dc942663
state6 = State("C1Destination",2,[false,true,true])

# ╔═╡ 609af5b0-017f-4f5f-bd02-6a16c73d6402
state7 = State("C2Destination",3,[false,false,false])

# ╔═╡ 0e1744dc-e44f-4974-95dc-ff4ded29f95b
state8 = State("EDestination",4,[false,false,true])

# ╔═╡ d13dad63-4c03-49ed-9a51-0e256330f000
# defining trasition model

# ╔═╡ aaa492f9-f86b-4c3f-b535-fcf6f1f5cc45
OfficeTModel = Dict()

# ╔═╡ fc72ff07-7609-4616-8b61-2598df49f138
push!(OfficeTModel, state1 => [(move_west,state2),(collect,state5),(remain,state1)])

# ╔═╡ 5438d4be-a33b-43bf-8fe8-ec9d30bf2c59
push!(OfficeTModel, state2 => [(move_west,state3),(collect,state6),(move_east,state1)])

# ╔═╡ 5ed5f938-ae89-485d-bb97-1df1227f00d7
push!(OfficeTModel, state3 => [(move_west,state4),(collect,state7),(move_east,state2)])

# ╔═╡ f451d947-9c46-4214-843d-b7224c2cc7d6
push!(OfficeTModel, state4 => [(remain,state4),(collect,state8),(move_east,state3)])

# ╔═╡ b8d72673-d49c-493f-b486-5f11aa092242
function userInput!(office, item)
	deleteat!(office, findfirst(x->x==item, office))
end

# ╔═╡ f58a1287-51b3-4a37-984e-702131a3d867
begin
	print("How many Offices are there in the Organisation?\n")
	office=readline()
	nOffice=parse(Int64, office)
	
	officeCollection=[]
	itemCollection=[]
	
	s=1
	i0=0
	i1=0
	i2=0
	i3=0
	stat1=0
	stat2=0
	stat3=0
	stat4=0
#=	st=0
	st=0
	st=0
	st=0
	st=0
=#	
#    global officeCollection = Array{String}(undef, nOffice)
	
    for i=1:nOffice
        print("What is the name of the Office ",i,"?\n")
        officeCo=readline()
		if officeCo=="W"
			global stat1=1
		elseif officeCo=="C1"
			global stat2=1
		elseif officeCo=="C2"
			global stat3=1
		elseif officeCo=="E"
			global stat4=1
       end
		push!(officeCollection,officeCo)
	end
	
    print("How many Items are there in the Office 1?\n")
    officeCO=readline()
    parse(Int64, officeCO)
	i0=officeCO
	if stat1==1
		
	    print("How many Items are there in the Office 2?\n")
        officeCO=readline()
        parse(Int64, officeCO)
	    i1=officeCO
		s+=1
		if stat2==1
				
			print("How many Items are there in the Office 3?\n")
            officeCO=readline()
            parse(Int64, officeCO)
	        i2=officeCO
			s+=1
	        if stat3==1
						
				print("How many Items are there in the Office 4?\n")
                officeCO=readline()
                parse(Int64, officeCO)
	            i3=officeCO
				s+=1
							
			end
		end
	end
	
#    global itemCollection = Array{String}(undef, nItm)
	
checkL=length(officeCollection)
	
    for i=1:checkL
        print("What position is the item in the Office ",officeCollection[i],"?\n1. First\n2. First & Second\n3. First, Second & Third\n4. Second\n5. Second & Third\n6. Third\n")
        itemC=readline()
			nItm=parse(Int64, itemC)
       end
end

# ╔═╡ 4fa4e32a-e06e-4be5-b34b-fcecb3005c62
println(" THE INITIAL STATE ")

# ╔═╡ c99f271b-2c90-4605-a373-2fabf0758052
println(state1)

# ╔═╡ ba4e6ad7-bdfe-4904-aa30-1a4ff41929ae
println(state2)

# ╔═╡ 6f588c62-a3ba-488a-b6ef-712903ce790a
println(state3)

# ╔═╡ bade3d8d-b244-4e30-8c9c-cc2543a48520
println(state4)

# ╔═╡ 9fdc2c7b-879a-41fe-86e1-e6b3963a63ca
println("")

# ╔═╡ 99efc2c1-3540-412a-b7c0-83d4661dda86
println(" Number of items in offices at INITIAL STATE ")

# ╔═╡ 6cc5cbd4-f099-4222-af6a-5db762052874
println("")

# ╔═╡ b0825a6a-a5cd-4e34-8e2f-e4e2ebe9b4e8
println("Number of items of office W are:")

# ╔═╡ ed527c93-c700-4c2f-bdd7-93df57cfb6ad
println(count([true,false,false]))

# ╔═╡ 3b8ee7b5-6fd5-48d4-8355-fd3828c94cc5
println("Number of items of office C1 are:")

# ╔═╡ 4d5664c2-be3c-460d-a19c-8545a05b3460
println(count([true,true,true]))

# ╔═╡ 478343e0-03f1-4d4b-a948-b053180fc9f4
println("Number of items of office C2 are:")

# ╔═╡ c0851273-4e6e-4616-824e-2b2bc35598a9
println(count([false,true,true]))

# ╔═╡ 3fc3d979-ee5e-44b7-a3b1-770e0a885b26
println("Number of items of office E are:")

# ╔═╡ 30e6d9ab-5308-4074-aa49-3d88547937e7
println(count([false,true,false]))

# ╔═╡ 90e6c560-09f8-4fc0-969c-fed07fd1c2c8
println("")

# ╔═╡ 7f429647-187f-459b-beef-755a34452c46
println(" THE GOAL STATE ")

# ╔═╡ d089d791-c6b6-4fd6-9240-7d45c65e01d2
println(state5)

# ╔═╡ 44848ceb-de78-441d-a654-e76a225707c1
println(state6)

# ╔═╡ eb6697c3-3404-4bce-a38e-ab19d1423f48
println(state7)

# ╔═╡ 923f0080-0514-4db6-906d-49b9a3b95c3e
println(state8)

# ╔═╡ 3522a6ee-9270-4825-a4d3-20690c2cfe1a
println("")

# ╔═╡ 30cea533-f7b2-4d7e-bbb4-966f485ba8e2
println("Total number of items left for collection in offices")

# ╔═╡ c8236e6f-7889-4929-be4d-540e3588ef2d
println("")

# ╔═╡ 27e6fe1b-5b9e-436c-88ee-88a87d7014af
println("Total Number of items left in office W are:")

# ╔═╡ 995288bc-254a-47ab-9576-d8d43d6cd61f
println(count([false,false,false]))

# ╔═╡ d331b706-0aaf-4701-a373-f8938e925c43
println("Total Number of items left in office C1 are:")

# ╔═╡ 387e36c5-1c3e-445b-9176-766d0aca9e1d
println(count([false,true,true]))

# ╔═╡ 222d0cf6-5a44-432d-970a-68f850049a56
println("Total Number of items left in office C2 are:")

# ╔═╡ 9e435de5-75df-4998-baf4-201af4da76f5
println(count([false,false,false]))

# ╔═╡ f13819a1-fe70-4ccd-b5de-4d63169b5bfc
println("Total Number of items left in office E are:")

# ╔═╡ e72f8f4c-7384-4b66-aef3-6c016aae465e
println(count([false,false,true]))

# ╔═╡ 4f83f0af-bfae-4027-98ae-e1e155552e3c
# A* search Algorithm

# ╔═╡ c592fed9-f910-4f7c-94dc-e8ac903cba5e
# heuristic function

# ╔═╡ 6ab2a17a-aa29-438b-95d2-220af859ee30
function heuristic(cell, goalState)
    return minAbsDistance(cell[1] - goalState[1]) + minAbsDistance(cell[2] - goalState[2])
end

# ╔═╡ ca705d78-4fce-4e03-b37b-45ed51e82e5b
# AstarSearch algorithm function with three arguments

# ╔═╡ 773c3be6-934d-4071-b47b-e4cc47ff494f
function AstarSearch(OfficeTModel,initialState, goalState)
	
	# setting result queue to empty list
    result = []
    heappush!(result, (0 + heuristic(initialState, goalState), 0, "", initialState))
	# setting vissited to empty set
    explored = Set()
	# remove last item from result queue if queue is not empty
    while length(result) != 0
        _, cost, path,current = heappop!(result)
		# add current to explored if current is equal to goalState
        if current == goalState
            return path, push!(explored, current)
        end
        if current in explored
            continue
        end
        push!(explored,current)
        for (direction, candidate) in OfficeTModel[current]
            heappush!(result, (cost + heuristic(candidate, goalState), cost + 1, path*direction, candidate))
        end
    end
	# return failure
    return "No Right Path found!"
end

# ╔═╡ f80810d5-89ce-47e4-b9e0-b5790218fb97
println("Press Enter to exit")

# ╔═╡ 623eb3f1-f430-4798-9bfc-a90e9c018175
x=readline()

# ╔═╡ Cell order:
# ╠═d555e035-af6b-4eaa-ae4d-b3f97118101c
# ╠═63bafe4c-7fe4-45d0-84b8-9b0853c6aead
# ╠═14e0e8ce-4ee4-44a9-9df1-33188fa99275
# ╠═88848721-d4b0-4419-80df-4d5a0acc1566
# ╠═7661c0ba-2df8-4c03-9188-d7bf8ae2b8bb
# ╠═eb07a06e-5c37-4bc1-94ea-6e3ee0bcb09e
# ╠═34e42f53-4040-4a12-bf27-e008acddbbe5
# ╠═5bab5f4f-26ef-4c63-ba66-0d7be96decbe
# ╠═288f6783-a1e8-40db-a05e-12b3ddc520b3
# ╠═14a04b6d-95a9-443b-a99a-501e7424a079
# ╠═133ae44b-9ce1-49e1-aea7-aed58556d880
# ╠═6a6d5473-6d98-430b-ac92-cc80b19dbc95
# ╠═e35ce7fb-8f05-437b-9d96-fceec2b10eae
# ╠═f8942517-95d8-4158-b23e-7e31235e5e8b
# ╠═9c9389df-fe76-4a39-abe4-548847b42ea2
# ╠═710531e8-12c0-40a4-aa7f-302184f7ce86
# ╠═9ecf52b7-c41c-4825-a1d9-50b54f61a9d8
# ╠═c52714ae-ebce-4917-9e94-6b5beff822c7
# ╠═b5a9c705-e564-4ade-ac50-b4c8dc942663
# ╠═609af5b0-017f-4f5f-bd02-6a16c73d6402
# ╠═0e1744dc-e44f-4974-95dc-ff4ded29f95b
# ╠═d13dad63-4c03-49ed-9a51-0e256330f000
# ╠═aaa492f9-f86b-4c3f-b535-fcf6f1f5cc45
# ╠═fc72ff07-7609-4616-8b61-2598df49f138
# ╠═5438d4be-a33b-43bf-8fe8-ec9d30bf2c59
# ╠═5ed5f938-ae89-485d-bb97-1df1227f00d7
# ╠═f451d947-9c46-4214-843d-b7224c2cc7d6
# ╠═b8d72673-d49c-493f-b486-5f11aa092242
# ╠═f58a1287-51b3-4a37-984e-702131a3d867
# ╠═4fa4e32a-e06e-4be5-b34b-fcecb3005c62
# ╠═c99f271b-2c90-4605-a373-2fabf0758052
# ╠═ba4e6ad7-bdfe-4904-aa30-1a4ff41929ae
# ╠═6f588c62-a3ba-488a-b6ef-712903ce790a
# ╠═bade3d8d-b244-4e30-8c9c-cc2543a48520
# ╠═9fdc2c7b-879a-41fe-86e1-e6b3963a63ca
# ╠═99efc2c1-3540-412a-b7c0-83d4661dda86
# ╠═6cc5cbd4-f099-4222-af6a-5db762052874
# ╠═b0825a6a-a5cd-4e34-8e2f-e4e2ebe9b4e8
# ╠═ed527c93-c700-4c2f-bdd7-93df57cfb6ad
# ╠═3b8ee7b5-6fd5-48d4-8355-fd3828c94cc5
# ╠═4d5664c2-be3c-460d-a19c-8545a05b3460
# ╠═478343e0-03f1-4d4b-a948-b053180fc9f4
# ╠═c0851273-4e6e-4616-824e-2b2bc35598a9
# ╠═3fc3d979-ee5e-44b7-a3b1-770e0a885b26
# ╠═30e6d9ab-5308-4074-aa49-3d88547937e7
# ╠═90e6c560-09f8-4fc0-969c-fed07fd1c2c8
# ╠═7f429647-187f-459b-beef-755a34452c46
# ╠═d089d791-c6b6-4fd6-9240-7d45c65e01d2
# ╠═44848ceb-de78-441d-a654-e76a225707c1
# ╠═eb6697c3-3404-4bce-a38e-ab19d1423f48
# ╠═923f0080-0514-4db6-906d-49b9a3b95c3e
# ╠═3522a6ee-9270-4825-a4d3-20690c2cfe1a
# ╠═30cea533-f7b2-4d7e-bbb4-966f485ba8e2
# ╠═c8236e6f-7889-4929-be4d-540e3588ef2d
# ╠═27e6fe1b-5b9e-436c-88ee-88a87d7014af
# ╠═995288bc-254a-47ab-9576-d8d43d6cd61f
# ╠═d331b706-0aaf-4701-a373-f8938e925c43
# ╠═387e36c5-1c3e-445b-9176-766d0aca9e1d
# ╠═222d0cf6-5a44-432d-970a-68f850049a56
# ╠═9e435de5-75df-4998-baf4-201af4da76f5
# ╠═f13819a1-fe70-4ccd-b5de-4d63169b5bfc
# ╠═e72f8f4c-7384-4b66-aef3-6c016aae465e
# ╠═4f83f0af-bfae-4027-98ae-e1e155552e3c
# ╠═c592fed9-f910-4f7c-94dc-e8ac903cba5e
# ╠═6ab2a17a-aa29-438b-95d2-220af859ee30
# ╠═ca705d78-4fce-4e03-b37b-45ed51e82e5b
# ╠═773c3be6-934d-4071-b47b-e4cc47ff494f
# ╠═f80810d5-89ce-47e4-b9e0-b5790218fb97
# ╠═623eb3f1-f430-4798-9bfc-a90e9c018175
