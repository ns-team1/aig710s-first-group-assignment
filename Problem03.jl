### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ bbc9fa43-360d-48a7-ba2c-e65f83b8c279
println("")

# ╔═╡ 76a18fcb-f93c-41fb-a77f-95fbc2d9390c
println("Constraint Satisfaction Problem")

# ╔═╡ 1a6f2937-45cb-4fdd-85b5-7f55ee6d1e5d
println("")

# ╔═╡ 9f018a8f-93e1-42b6-98bd-1c4d770cbcf6
println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

# ╔═╡ 8b97ef5f-e58a-4b31-9a91-814d39f130e2
println("")

# ╔═╡ d31adf42-b009-11eb-20a4-45899b4ce88d
col_mapping = Dict(:x1 =>1,:x2 =>2,:x3 =>3,:x4 =>4,:x5 =>5,:x6 =>6,:x7 =>7)

# ╔═╡ dfc77617-d11d-4f95-8135-51730e5f0bb4
function print_sol(d::Dict, pre=1)
    for (k,v) in d
        if typeof(v) <: Dict
            s = "$(repr(k)) => "
            println(join(fill(" ", pre)) * s)
            print_sol(v, pre+1+length(s))
        else
            println(join(fill(" ", pre)) * "$(repr(k)) => $(repr(v))")
        end
    end
    nothing
end


# ╔═╡ 3369a5a8-d8f2-44bb-8d0b-a1a9a36c077a
struct Constraint
    variables::Vector{String}
    place1::String
    place2::String
    function Constraint(variables::Vector{String}, place1, place2)
        return new(variables, place1, place2)
    end
end

# ╔═╡ e384566f-7d51-46a9-b9ea-d6ebc9a4ef82
function satisfied(constraint::Constraint, assignment)::Bool
    if(!haskey(assignment, constraint.place1) || !haskey(assignment, constraint.place2))
        return true
    end
    return assignment[constraint.place1] != assignment[constraint.place2]
end

# ╔═╡ fdca227a-4366-4054-87f4-73b8c16c2671
struct CSP
    variables::Vector
    domains::Dict{String, Vector}
    constraints::Dict{String, Vector{}}
	
	function CSP(vars, doms, cons=Dict())
        variables = vars
        domains = doms
        constraints = cons
        for var in vars
            constraints[var] = Vector()
            if (!haskey(domains, var))
                error("Every variable should have a domain assigned to it.")
            end
        end
        return new(variables, domains, constraints)
    end
end

# ╔═╡ dfc5c215-8410-43a7-a6f5-5502697fbe0d
function add_constraint(csp:: CSP, constraint::Constraint)
    for vari in constraint.variables
        if (!(vari in csp.variables))
            error("Variable in constraint not in CSP")
        else
            push!(csp.constraints[vari], constraint)
        end
    end
end

# ╔═╡ 275d7aed-9ff0-46db-9cab-fd4ef0c7b46a
function check_consistent(csp:: CSP, variable, assignment)::Bool
    for constraint in csp.constraints[variable]
        if (!satisfied(constraint, assignment))
            return false
        end
        return true
    end
end

# ╔═╡ e8ee6fa3-5fb9-4963-bbd2-ceb38065f804
function backtracking_search(csp:: CSP,  assignment=Dict(), path=Dict())::Union{Dict,Nothing}
    
     if length(assignment) == length(csp.variables)
        return assignment
     end

    unassigned::Vector{String} = []
    for v in csp.variables
        if (!haskey(assignment, v))
            push!(unassigned, v)
        end
    end

    # get the every possible domain value of the first unassigned variable
    first = unassigned[1]
    # println(first)
    # pretty_print(csp.domains)
    for value in csp.domains[first]
        local_assignment = deepcopy(assignment)
        local_assignment[first] = value
        # if we're still consistent, we recurse (continue)
        if check_consistent(csp, first, local_assignment)
            # forward checking, prune future assignments that will be inconsistent
            for un in unassigned
                ass = deepcopy(local_assignment)
                for (i, val) in enumerate(csp.domains[un])
                    ass[un] = val
                    if un != first
                        if(!check_consistent(csp, un, ass))
                            deleteat!(csp.domains[un], i)
                        end
                    end
                end
            end
            path[first] = csp.domains
            # println("reduced")
            # print_sol(csp.domains)
            result = backtracking_search(csp, local_assignment)
            #backtrack if nothing is found
            if result !== nothing
                print_sol(path)
                return result
            end
        end
    end
    return nothing
end

# ╔═╡ 729db5d3-e158-432f-8d94-651d8837ac59
begin
variables = ["X1", "X2", "X3", "X4", "X5", "X6", "X7"]

domains = Dict()
for variable in variables
    domains[variable] = ["1", "2", "3", "4"]
end

game = CSP(variables, domains)
add_constraint(game, Constraint(["X1", "X2"], "X1", "X2"))
add_constraint(game, Constraint(["X1", "X3"], "X1", "X3"))
add_constraint(game, Constraint(["X1", "X5"], "X1", "X5"))
add_constraint(game, Constraint(["X1", "X6"], "X1", "X6"))
add_constraint(game, Constraint(["X2", "X5"],"X2", "X5"))
add_constraint(game, Constraint(["X3", "X4"], "X3", "X4"))
add_constraint(game, Constraint(["X4", "X5"], "X4", "X5"))
add_constraint(game, Constraint(["X4", "X6"], "X4", "X6"))
add_constraint(game, Constraint(["X5", "X6"], "X5", "X6"))
add_constraint(game, Constraint(["X6", "X7"], "X6", "X7"))

solution = backtracking_search(game)
if solution != Nothing
		println("")
    print("solution is: ", solution)
else
    println("There's no solution")
end
end

# ╔═╡ Cell order:
# ╠═bbc9fa43-360d-48a7-ba2c-e65f83b8c279
# ╠═76a18fcb-f93c-41fb-a77f-95fbc2d9390c
# ╠═1a6f2937-45cb-4fdd-85b5-7f55ee6d1e5d
# ╠═9f018a8f-93e1-42b6-98bd-1c4d770cbcf6
# ╠═8b97ef5f-e58a-4b31-9a91-814d39f130e2
# ╠═d31adf42-b009-11eb-20a4-45899b4ce88d
# ╠═dfc77617-d11d-4f95-8135-51730e5f0bb4
# ╠═3369a5a8-d8f2-44bb-8d0b-a1a9a36c077a
# ╠═e384566f-7d51-46a9-b9ea-d6ebc9a4ef82
# ╠═fdca227a-4366-4054-87f4-73b8c16c2671
# ╠═dfc5c215-8410-43a7-a6f5-5502697fbe0d
# ╠═275d7aed-9ff0-46db-9cab-fd4ef0c7b46a
# ╠═e8ee6fa3-5fb9-4963-bbd2-ceb38065f804
# ╠═729db5d3-e158-432f-8d94-651d8837ac59
